<!DOCTYPE html>
<head><title>Calculator</title></head>
<body>
     

<form action= "LukeSorensenCalculator.php" method="GET">
    
    <h2> CALCULATOR </h2>
    
    <br>
	
    <label> First Number: <input type="text" name="num1"> </label>
    
	<br>
    <br>
    
    Function:
    <label> x : <input type="radio" name="funct" value="x"> </label>
    <label> / : <input type="radio" name="funct" value="/"> </label>
    <label> + : <input type="radio" name="funct" value="+"> </label>
    <label> - : <input type="radio" name="funct" value="-"> </label>
    
    <br>
    <br>
    
    <label> Second Number: <input type="text" name="num2"> </label>
    
    <br>
    <br>
   	
    <input type="submit" value="Calculate" />
    
    <br>
    <br>
	       
</form>

<?php


if(isset($_GET['num1']) && isset($_GET['num2'])){

    if(!(is_numeric($_GET['num1']) && is_numeric($_GET['num2']))){        
         echo "Please put proper inputs";
    }
    
    elseif($_GET['funct']=="x"){
        echo $_GET['num1']*$_GET['num2'];
    }
    
    elseif($_GET['funct']=="/"){  
        if($_GET['num2']==0){           
            echo "<h1>CAN'T DIVIDE BY ZERO</h1>";
            
        } else{       
            echo $_GET['num1']/$_GET['num2'];   
        }
        
    } elseif($_GET['funct']=="+"){        
        echo $_GET['num1']+$_GET['num2'];
        
    } elseif($_GET['funct']=="-"){       
        echo $_GET['num1']-$_GET['num2'];
        
    } else{
        echo 'Please choose a function';
    }

}

?>

</body>

</html>